﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace NumberSearch.Tests
{
    [TestClass]
    public class NumberFinderTests
    {
        [TestMethod]
        public void FindNumbers_lineANDlist_1numbers()
        {
            //Arrange
            string line = "a888ca";
            List<string> list = new List<string>() { "a", "a888ca" };
            int expected = 1;

            //Act
            IEnumerable<string> actual = NumberFinder.FindNumbers(line, list);

            //Assert
            Assert.AreEqual(expected, actual.Count());
        }


        [TestMethod]
        public void FindNumbers_lineANDlist_NotFoundreturns()
        {
            //Arrange
            string line = "р546рр";
            List<string> list = new List<string>() { "a", "a888ca" };
            int expected = 0;

            //Act
            IEnumerable<string> actual = NumberFinder.FindNumbers(line, list);

            //Assert
            Assert.AreEqual(expected, actual.Count());
        }
    }
}
