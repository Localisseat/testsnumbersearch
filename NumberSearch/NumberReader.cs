﻿using System;
using System.Collections.Generic;
using System.IO;

public class NumberReader
{
    private const string path = @"hta.txt";

    public static List<string> ReadFile()
    {
        List<string> listNumber = new List<string>();
        using (StreamReader file = new StreamReader(path, System.Text.Encoding.Default))
        {
            string line;

            while ((line = file.ReadLine()) != null)
            {
                listNumber.Add(line);
            }
        }
            
        return listNumber;
    }
}
