﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberSearch
{
    public class NumberFinder
    {
        public static IEnumerable<string> FindNumbers(string line, List<string> numbers)
        {
            return numbers.Where(str => str.Contains(line));
        }
    }
}
