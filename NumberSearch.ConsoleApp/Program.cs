﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberSearch.ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {

            try
            {
                List<string> listNumber = NumberReader.ReadFile();
                ConsoleKeyInfo keypress;
                do
                {
                    Console.WriteLine("Введите автомобильный номер");
                    string name = Console.ReadLine();

                    var result = NumberFinder.FindNumbers(name, listNumber);
                    Console.WriteLine($"Количество найденых номеров: {result.Count()}");

                    foreach(string res in result)
                    {
                        Console.WriteLine(res);
                    }

                    Console.WriteLine("Ввести еще один автомобильный номер?");
                    keypress = Console.ReadKey(); // считать данные о нажатых клавишах
                } while (keypress.KeyChar != 0x1B);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
